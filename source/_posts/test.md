---
title: Blog Introduction 2
date: 2020/12/29
excerpt: Bienvenidos a Eduo este es el primer blog publicado esperamos que sea de su agrado 😄.
photos: https://bulma.io/images/expo/getbedtimestories-672x420.jpg
author: '@Frenrihr'
icon: fa-commenting
tags: ["ThisIsATag", "Intro", "Welcome"]
categories: ["Configuration", "Hexo"]
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```
nose que poner aqui